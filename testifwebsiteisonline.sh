#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "You must and can pass only one argument. For string with spaces put it on quotes"
    exit 0
fi

url_parameter=$1
counter=1

if [[ $url_parameter == *"https://"* ]]; then
 working_url=$url_parameter
 pure_url=${url_parameter#"https://"}
else 
 working_url=("https://$url_parameter")
 pure_url=$url_parameter
fi

pure_url="${pure_url//[\/=?]/_}"


#echo 'to test >>>>>' + $working_url
#echo 'pure url >>>>>' + $pure_url
#echo "COUNTER ->$counter"


# Chcke website
#wget -q --spider $working_url
wget -q --spider --tries=5 $working_url


if [ $? -eq 0 ]; then
    echo "All good! Website is online"
    echo "0" >$pure_url
else
    echo "Oh no! Website is offline!"
    #Get file content, it it is exists, and increment it
    counter=$(<$pure_url)
    counter=$((counter + 1))
    echo $counter > $pure_url
    if(($counter >= 5)); then
           echo "0" >$pure_url
           echo "send notification!!!!"
    fi
fi



# for the future advance version, to check different ports and services

# if nc -z $server 22 2>/dev/null; then
#    echo "$server ✓"
# else
#    echo "$server ✗"
# fi
