# Websites pulse checker

This is the very small bash code to check list of different websites and send notifications if any of them is down.


## Getting started

Copy files, and change both .sh files to be executable, and run the checkingwebsites.sh

The best way to do it is from the cronjob, which will run on every 5-15 min for example.
Notification will be send after 5 times checked offline (you can change that in the code)


```
cd existing_repo
git remote add origin https://gitlab.tudelft.nl/atomic1/websites-pulse-checker.git
git branch -M main
git push -uf origin main
```
## Project status
In progress (90% done)
